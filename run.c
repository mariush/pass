#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include <time.h>

void random_string(char * string, size_t length)
{
  /* Seed number for rand() */
  srand((unsigned int) time(0));

  /* ASCII characters 33 to 126 */
  unsigned int num_chars = length - 1;
  unsigned int i;
  for (i = 0; i < num_chars; ++i)
    {
      string[i] = rand() % (126 - 33 + 1) + 33;
    }

  string[num_chars] = '\0';
}

// Generate random files
void random_files()
{
    char rstring[30];
    for(int i = 0; i <= 5; i++)
    {
        random_string(rstring,20);
        FILE *file = fopen(rstring, "w");
    }
}

void pwd_gen(char * pwd, char * s)
{
	int x = 0, key = 2;

	// s = filename

	for(int i = 0; i < strlen(s); i++)
		if(i % 2 == 1)
		{	pwd[x] = s[i];
			x++;
		}

	// Caesar chiper
    for (int i = 0; i<strlen(pwd); i++)
    {

        //for digits
        if (pwd[i] > 47 && pwd[i] < 58 )
        {
            int k = key;
            while (k>10) k = k-10;
            if (pwd[i] + k > 58) pwd[i] = pwd[i] - 10 + k;
                else    pwd[i] = pwd[i]+k;
        }

        //for uppercase letters
        if (pwd[i] > 64 && pwd[i] < 91 )
        {
            int k = key;
            while (k>26) k = k-26;
            if (pwd[i] + k > 91 ) pwd[i] = pwd[i] - 26 + k;
                else    pwd[i] = pwd[i]+k;
        }

        //for lowercase letters
        if (pwd[i] > 96 && pwd[i] < 123 )
        {
            int k = key;
            while (k>26) k = k-26;
            if (pwd[i] + k > 123) pwd[i] = pwd[i] - 26 + k;
                else    pwd[i] = pwd[i]+k;
        }
    }

}

int main ()
{
	char s[50]="FZXC0wg0LvaJ6atJJx2a9vnfSFj4QhlOgb", rstring[80], pwd[40], u_pwd[40];


	// Generate the file's name.

	random_string(rstring, 10);

	// Create the file and write the content.
	FILE *file = fopen(s, "w");
	fprintf(file,"%s", rstring);

    // Generate random files
    random_files();

	// Extract the password
	pwd_gen(pwd,s);

    printf ("\nEnter the password: ");
    scanf ("%s", u_pwd);

    if(strcmp(pwd, u_pwd ) == 0)
        printf("53cr37 m355@93.\n");
    else
        printf("Nice try.\n");

}
